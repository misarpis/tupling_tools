import argparse
parser = argparse.ArgumentParser(description="Submit Dirac jobs for MC. "\
                                 "Even though it is possible to submit jobs for several channels, years and eventtypes at once, it is recommended to submit them one by one!")
parser.add_argument('-c','--channel',   dest='channel',   required=True, nargs='+', choices=['LcD0K', 'LcDs'],        help='Decay Channel')
parser.add_argument('-e','--eventType', dest='eventType', required=True, nargs='+', choices=['15396000', '15396000v1', '15296003', '15396200', '15496200', '15296004', '15496220', '15296020'], help='MC event type')
parser.add_argument('-y','--year',      dest='years',     required=True, nargs='+', choices=['15', '16', '17', '18'], help='Year of data-taking')
parser.add_argument('-t','--CPUTime',   dest='CPUTime',   type=int, default=7200,                                     help='set CPUTime for Dirac backend')
parser.add_argument('-s','--split',     dest='split',     type=int, default=30,                                       help='Number of input files per subjob')
args = parser.parse_args()

bkk = {
    "15396000": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20150724",
            "COND":
            "sim-20161124-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20150724",
            "COND":
            "sim-20161124-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20180411-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20180411-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190128-vc-mu100"
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190128-vc-md100"
        },
    },
    "15396000v1": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09h/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09h/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-mu100"
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396000/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-md100"
        },
    },
    "15296003": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20150724",
            "COND":
            "sim-20161124-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20150724",
            "COND":
            "sim-20161124-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20180411-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20180411-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190128-vc-mu100"
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190128-vc-md100"
        },
    },
    "15396200": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20180411-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20180411-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190128-vc-md100"  # (sic)
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190128-vc-md100"
        }
    },
    "15496200": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x6138160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x6138160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-mu100"
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09k-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15496200/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-md100"
        },
    },
    "15296004": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-mu100"
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15296004/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-md100"
        },
    },
    "15496220": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-mu100"
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15496220/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-md100"
        },
    },
    "15296020": {
        "15U": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-mu100"
        },
        "15D": {
            "path":
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20161124-vc-md100"
        },
        "16U": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-mu100"
        },
        "16D": {
            "path":
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20170721-2-vc-md100"
        },
        "17U": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-mu100"
        },
        "17D": {
            "path":
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-1-vc-md100"
        },
        "18U": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-mu100"
        },
        "18D": {
            "path":
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15296020/ALLSTREAMS.DST",
            "DDDB":
            "dddb-20170721-3",
            "COND":
            "sim-20190430-vc-md100"
        },
    },
}

def submit_it(channel, evtt, year, polarity):
  dv_version = "v46r5"
  platform = "x86_64_v2-centos7-gcc11-opt"
  # if year == "15" or year == "16":
  #     dv_version = "v44r10p5"
  #     platform = "x86_64-centos7-gcc7-opt"
  try:
    myApp = prepareGaudiExec("DaVinci", dv_version, ".")
  except:
    myApp = GaudiExec()
  myApp.platform = platform
  myApp.directory = "./v46r5"

  cf = "tuple.py"
  sd = bkk[evtt][year+polarity]
  dataset = BKQuery(sd["path"]).getDataset()
  myApp.extraOpts = "from Configurables import DaVinci;from Gaudi.Configuration import *;DaVinci(DataType='20{0}',DDDBtag='{1}',CondDBtag='{2}',TupleFile='{3}_{0}{4}.root');importOptions('{5}');".format(year, sd["DDDB"], sd["COND"], evtt, polarity, cf)
  j = Job(name=evtt+channel+year+polarity)
  j.inputfiles = [cf, "./standard_functors.py", "./dfrombtools.py"]
  j.application = myApp
  j.inputdata = dataset[0:None]
  j.backend = Dirac()
  j.backend.settings['CPUTime'] = args.CPUTime
  j.outputfiles = [DiracFile('*.root')]
  j.splitter = SplitByFiles(filesPerJob=args.split)
  j.submit()

for yr in args.years:
  for ev in args.eventType:
    for c in args.channel:
      for p in ["U","D"]:
        submit_it(c, ev, yr, p)
